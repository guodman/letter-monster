extern crate rustbox;
extern crate rand;

use rustbox::RustBox;
use rustbox::Color;
use rustbox::Style;
use rustbox::keyboard::Key;
use std::time::Duration;
use std::char;
use rand::distributions::Range;
use rand::distributions::IndependentSample;

pub struct Monster {
    x: usize,
    y: usize,
    start_y: usize,
    variety: i32,
    move_cooldown: i32,
    tail_direction: bool,
}

pub struct Board {
    m: Monster,
    rng: rand::ThreadRng,
    punching: i32,
}

fn main() {
    run_loop(
        Board {
            m: Monster {
                x: 10,
                y: 30,
                start_y: 30,
                variety: 109,
                move_cooldown: 0,
                tail_direction: true,
            },
            rng: rand::thread_rng(),
            punching: 0,
        });
}

fn run_loop(mut board: Board) {
    let mut rustbox = match RustBox::init(Default::default()) {
        Result::Ok(v) => v,
        Result::Err(e) => panic!("{}", e),
    };

    let mut c: Key = Key::Home;
    let mut loop_count = 0;
    while c != Key::Esc {
        match rustbox.peek_event(Duration::new(0, 50), false) {
            Ok(rustbox::Event::KeyEvent(key)) => {
                match key {
                    Key::Char(k) => c = Key::Char(k),
                    Key::Esc => c = Key::Esc,
                    _ => c = Key::Home,
                }
            },
            _ => {
                c = Key::Home;
            }
        }
        update(30, &mut board, c);
        render(&mut rustbox, &board);
        std::thread::sleep(Duration::from_millis(30));
        loop_count = loop_count + 1;
    }
}

fn render(rustbox: &mut RustBox, board: &Board) {
    rustbox.clear();
    let c: char = char::from_u32(board.m.variety as u32).unwrap_or('-');
    if board.punching == 0 {
        render_bed(rustbox, 10, 10);
        render_monster(rustbox, board.m.x, board.m.y, c, board.m.tail_direction);
    } else if board.punching > 0 {
        render_bed_empty(rustbox, 10, 10);
        render_monster(rustbox, board.m.x, board.m.y, c, board.m.tail_direction);
        render_punch(rustbox, board.m.x + 6, board.m.y);
    }
    rustbox.present();
}

fn update(delta: i32, board: &mut Board, typed_character: Key) {
    if board.m.y > 7 && board.punching == 0 && board.m.move_cooldown <= 0 {
        board.m.y = board.m.y - 1;
        board.m.move_cooldown = 1000;
        board.m.tail_direction = !board.m.tail_direction;
    }
    board.m.move_cooldown -= delta;

    if typed_character == Key::Char(board.m.variety as u8 as char) {
        board.punching = 3000;
    }

    if board.punching > 1 {
        board.punching = board.punching - delta;
        if board.punching <= 0 {
            board.punching = 0;
            // select a new random character for the monster
            let between = Range::new(97, 123); // letters
            //let between = Range::new(48, 58); // numbers
            let next: i32 = between.ind_sample(&mut board.rng);
            board.m.variety = next;
            board.m.y = board.m.start_y;
            board.punching = 0
        }
    }
}

/* 432101234
Standard monster:
-1  M     M
 0 MM  M  MM
 1 MMM M MMM
 2  MMMMMMM
 3   MMMMM
 4    MMM
 5     M
 6    M
 7    M
 8     M
 9      M
*/

fn render_monster(rustbox: &mut RustBox, x: usize, y: usize, c: char, swing: bool) {
    let style = Style::empty();
    let fg = Color::Red;
    let bg = Color::Default;
    let s = &c.to_string().to_uppercase();
    rustbox.print(x-3, y-1, style, fg, bg, s);
    rustbox.print(x+3, y-1, style, fg, bg, s);
    rustbox.print(x-4, y, style, fg, bg, s);
    rustbox.print(x-3, y, style, fg, bg, s);
    rustbox.print(x,   y, style, fg, bg, s);
    rustbox.print(x+3, y, style, fg, bg, s);
    rustbox.print(x+4, y, style, fg, bg, s);
    rustbox.print(x-4, y+1, style, fg, bg, s);
    rustbox.print(x-3, y+1, style, fg, bg, s);
    rustbox.print(x-2, y+1, style, fg, bg, s);
    rustbox.print(x,   y+1, style, fg, bg, s);
    rustbox.print(x+2, y+1, style, fg, bg, s);
    rustbox.print(x+3, y+1, style, fg, bg, s);
    rustbox.print(x+4, y+1, style, fg, bg, s);
    rustbox.print(x-3, y+2, style, fg, bg, s);
    rustbox.print(x-2, y+2, style, fg, bg, s);
    rustbox.print(x-1, y+2, style, fg, bg, s);
    rustbox.print(x,   y+2, style, fg, bg, s);
    rustbox.print(x+1, y+2, style, fg, bg, s);
    rustbox.print(x+2, y+2, style, fg, bg, s);
    rustbox.print(x+3, y+2, style, fg, bg, s);
    rustbox.print(x-2, y+3, style, fg, bg, s);
    rustbox.print(x-1, y+3, style, fg, bg, s);
    rustbox.print(x,   y+3, style, fg, bg, s);
    rustbox.print(x+1, y+3, style, fg, bg, s);
    rustbox.print(x+2, y+3, style, fg, bg, s);
    rustbox.print(x-1, y+4, style, fg, bg, s);
    rustbox.print(x,   y+4, style, fg, bg, s);
    rustbox.print(x+1, y+4, style, fg, bg, s);
    rustbox.print(x,   y+5, style, fg, bg, s);
    if swing {
        rustbox.print(x-1, y+6, style, fg, bg, s);
        rustbox.print(x-1, y+7, style, fg, bg, s);
        rustbox.print(x,   y+8, style, fg, bg, s);
        rustbox.print(x+1, y+9, style, fg, bg, s);
    } else {
        rustbox.print(x+1, y+6, style, fg, bg, s);
        rustbox.print(x+1, y+7, style, fg, bg, s);
        rustbox.print(x,   y+8, style, fg, bg, s);
        rustbox.print(x-1, y+9, style, fg, bg, s);
    }
}

/*
Child in bed
-5 O-----O
-4 |  O  |
-3 | /-\ |
-2 |  |  |
-1 | / \ |
 0 O-----O
*/
fn render_bed(rustbox: &mut RustBox, x: usize, y: usize) {
    let style = Style::empty();
    let bed_color = Color::Yellow;
    let fg = Color::White;
    let bg = Color::Default;
    rustbox.print(x-3, y-5, style, bed_color, bg, "O-----O");
    rustbox.print(x-3, y-4, style, bed_color, bg, "|     |");
    rustbox.print(x-3, y-3, style, bed_color, bg, "|     |");
    rustbox.print(x-3, y-2, style, bed_color, bg, "|     |");
    rustbox.print(x-3, y-1, style, bed_color, bg, "|     |");
    rustbox.print(x-3, y,   style, bed_color, bg, "O-----O");

    rustbox.print(x-1, y-4, style, fg, bg, " O");
    rustbox.print(x-1, y-3, style, fg, bg, "/-\\");
    rustbox.print(x-1, y-2, style, fg, bg, " |");
    rustbox.print(x-1, y-1, style, fg, bg, "/ \\");
}

fn render_bed_empty(rustbox: &mut RustBox, x: usize, y: usize) {
    let style = Style::empty();
    let fg = Color::White;
    let bg = Color::Default;
    rustbox.print(x-3, y-5, style, fg, bg, "O-----O");
    rustbox.print(x-3, y-4, style, fg, bg, "|     |");
    rustbox.print(x-3, y-3, style, fg, bg, "|     |");
    rustbox.print(x-3, y-2, style, fg, bg, "|     |");
    rustbox.print(x-3, y-1, style, fg, bg, "|     |");
    rustbox.print(x-3, y,   style, fg, bg, "O-----O");
}

/*
Punching Monster
-1  M     M
 0 MM  M  MM   O
 1 MMM M MMM ---
 2  MMMMMMM    |
 3   MMMMM    / \
 4    MMM
 5     M
 6    M
 7    M
 8     M
 9      M

0   O
1 ---
2   |
3  / \
*/
fn render_punch(rustbox: &mut RustBox, x: usize, y: usize) {
    let style = Style::empty();
    let fg = Color::White;
    let bg = Color::Default;
    rustbox.print(x, y,   style, fg, bg, &"  O".to_string());
    rustbox.print(x, y+1, style, fg, bg, &"---".to_string());
    rustbox.print(x, y+2, style, fg, bg, &"  |".to_string());
    rustbox.print(x, y+3, style, fg, bg, &" / \\".to_string());
}
